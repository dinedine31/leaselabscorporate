  var mobileBreak = 640;
  var setHeight = function() {
    var wheight = $(window).height();
    if ($(window).width() > mobileBreak) {
      $('#site-sidebar').height(wheight);
    } else {
      $('#site-sidebar').css('height', 'auto');
    }
  }

  var setResize = function() {
    $(window).on('resize', function() {
      wheight = $(window).height();
      if ($(window).width() > mobileBreak) {
        $('#site-sidebar').height(wheight);
      } else {
        $('#site-sidebar').css('height', 'auto');
      }
    });
  }

$(document).ready(function() {
  setHeight();
  setResize();

  // Hack to get off-canvas .menu-icon to fire on iOS
  $('.menu-icon').click(function() {
    false
  });

  $(function() {
    $('#full-bg').maximage({
      cycleOptions: {
        fx: 'fade',
        speed: 1000,
        timeout: 5000,
        prev: '#arrow_left',
        next: '#arrow_right',
        pause: false
      },
      fillElement: '#site-pages',
      backgroundSize: 'cover',
    });
  });

    $(".comment").shorten();




  /* Sticky Footer Foundation fix */
  $(".footer, .push").height($(".footer .row").height() + "px");
  $(".wrapper").css({
    'margin-bottom': (-1 * $(".footer .row").height()) + "px"
  });
  window.onresize = function() {
    $(".footer, .push").height($(".footer .row").height() + "px");
    $(".wrapper").css({
      'margin-bottom': (-1 * $(".footer .row").height()) + "px"
    });
  }



});


/* EXAMPLE OPTIONS -- IF OPTIONS REMAIN UNSET, GMAPS WILL USE DEFAULT SETTINGS

function GMapsSetOptions(){
    return {
        map_id_attr: 'map', // ID ATTRIBUTE OF GOOGLE MAP (USUALLY "#MAP")
        location_button_class_name: 'location_title', // CLASS NAME OF BUTTON ACTIVATING A LOCATION ON CLICK
        poi_button_class_name: 'poi_title', // CLASS NAME OF BUTTON ACTIVATING A POI ON CLICK
        
        enable_property_marker_click: boolean, // ENABLE ACTIVATION OF INFO-WINDOW ON CLICK FOR PROPERTY MARKER
        poi_link_activates_marker: boolean, // ENABLE ACTIVATION OF INFO-WINDOW ON CLICK FOR POI MARKER (INSTEAD OF CLICK-THROUGH TO URL/HREF)
        activate_property_marker: boolean, // ACTIVATE INFO-WINDOW ON PAGE LOAD FOR PROPERTY MARKER
        activate_first_location: boolean, // ACTIVATE FIRST LOCATION ON PAGE LOAD
        activate_first_poi: boolean, // ACTIVATE FIRST POI FOLLOWING ACTIVATION OF CONTAINING LOCATION
        marker_anchor_point: [0,0], // +/- INTEGER (ARRAY) ANCHOR COORDINATES FOR MARKER

        map_style: [{}], // (JSON) (https://developers.google.com/maps/documentation/javascript/reference?csw=1#MapTypeStyle - AND - http://gmaps-samples-v3.googlecode.com/svn/trunk/styledmaps/wizard/index.html)
        
        map_type: {}, // (OBJECT) (https://developers.google.com/maps/documentation/javascript/reference#MapTypeId)
        marker_animation: {}, // (OBJECT) (https://developers.google.com/maps/documentation/javascript/reference#Animation)
        
        map_fit_bounds_to_pois: boolean, //
        map_zoom_range: [0,14], // + INTEGER
        map_zoom_initial: 12, // + INTEGER
        
        map_center_offset_px: [0,0], // + INTEGER
        map_center_offset_zoom: 0, // +/- INTEGER

        //# CONTROLS OPTIONS #//
        scroll_wheel_control: boolean, // (https://developers.google.com/maps/documentation/javascript/reference#MapOptions)
        street_view_control: {} |or| boolean, // (https://developers.google.com/maps/documentation/javascript/reference#MapOptions)
        overview_control: {} |or| boolean, // (https://developers.google.com/maps/documentation/javascript/reference#MapOptions)
        rotate_control: {} |or| boolean, // (https://developers.google.com/maps/documentation/javascript/reference#MapOptions)
        scale_control: {} |or| boolean, // (https://developers.google.com/maps/documentation/javascript/reference#MapOptions)
        zoom_control: {} |or| boolean, // (https://developers.google.com/maps/documentation/javascript/reference#MapOptions)
        type_control: {} |or| boolean, // (https://developers.google.com/maps/documentation/javascript/reference#MapOptions)
        pan_control: {} |or| boolean, // (https://developers.google.com/maps/documentation/javascript/reference#MapOptions)
        
        //# GMAPS STATIC OPTIONS #//
        map_static_pin_src: 'http://code.l3as3labs.com/assets/gmaps/markers/generic/main-pin.png',
        map_static_for_mobile: boolean, // ALTER SCRIPT FUNCTIONALITY FOR MOBILE DEVICES
        map_static_id_attr: 'map_static', // ID ATTRIBUTE OF GOOGLE MAP (USUALLY "#MAP_STATIC")
        map_static_dimensions: '640x640', //
        map_static_scale: 2, // + INTEGER
        
        poi_content: function(poi){
            return [HTML-TEMPLATE-CODE];
        } //
    };
}

*/

function GMapsSetOptions() {
  google.maps.visualRefresh = true;

  /*#
    https://developers.google.com/maps/documentation/javascript/basics#VisualRefresh
    https://developers.google.com/maps/documentation/javascript/reference: "You can enable the new Google Maps visual refresh by setting the global property google.maps.visualRefresh to true in your code. This will cause the map to render with newly styled base map tiles, markers, info windows and controls. This will become the default style in the 3.14 release."
    #*/

  return {
    enable_property_marker_click: false,
    poi_link_activates_marker: true,
    activate_property_marker: false,
    activate_first_location: true,
    activate_first_poi: false,

    scroll_wheel_control: false,
    street_view_control: false,
    overview_control: false,
    rotate_control: false,
    scale_control: false,
    pan_control: false,
    type_control: {
      position: google.maps.ControlPosition.TOP_RIGHT
    },
    zoom_control: {
      position: google.maps.ControlPosition.RIGHT_TOP,
      style: google.maps.ZoomControlStyle.SMALL
    },
    map_zoom_range: [4, 20], // + INTEGER
    map_zoom_initial: 12, // + INTEGER
    mobile_breakpoint: 641, // + INTEGER 

    map_static_pin_src: 'markers/main_pin.png',

    poi_content: function(poi) {
      var has_full_address = (poi.address && poi.city && poi.state && poi.zip);

      var html_str = '';

      html_str += '<div class="gmaps_info_window">';
      html_str += (poi.name) ? '<h4 class="name">' + poi.name + '</h4>' : '';
      html_str += (poi.description) ? '<p class="description">' + poi.description + '</p>' : '';
      html_str += (has_full_address) ? '<p class="location"><span class="address">' + poi.address + '</span><br /><span class="city">' + poi.city + '</span><span>,&nbsp;</span><span class="state">' + poi.state + '</span><span>&nbsp;</span><span class="zip">' + poi.zip + '</span></p>' : '';
      html_str += (has_full_address || poi.href) ? '<p class="cta">' : '';
      html_str += (poi.href) ? '<a href="' + poi.href + '" title="' + poi.name + '" target="_blank">Visit Website</a><span>&nbsp;</span>' : '';
      html_str += (has_full_address) ? '<a href="http://maps.google.com/maps?daddr=' + poi.address + ',' + poi.city + ',' + poi.state + ',' + poi.zip + '" title="' + poi.name + '" target="_blank">Get Directions</a>' : '';
      html_str += (has_full_address || poi.href) ? '</p>' : '';
      html_str += '</div>';

      return html_str;
    }
  };
}

function propInit() {
  trace('propInit()');
}